import os


def test_cli():
    from lusid_express import load_config
    os.system("rm -rf lusid_express/config.yaml")
    #run the python -m lusid_express --enable vars command
    os.system("python -m lusid_express --enable vars")
    #check if the config.yaml file has been created with the vars feature enabled
    assert os.path.exists("lusid_express/config.yaml")
    with open("lusid_express/config.yaml", "r") as f:
        config = load_config()
        assert 'vars' in config['features'] 
        
        os.system("python -m lusid_express --disable vars")
        config = load_config()
        assert 'vars' not in config['features']
        os.system("python -m lusid_express --enable vars magic")
        config = load_config()
        assert 'vars' in config['features']
        assert 'magic' in config['features']
        os.system("python -m lusid_express --disable vars magic")
        config = load_config()
        assert 'vars' not in config['features']
        assert 'magic' not in config['features']

        