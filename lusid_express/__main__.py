import argparse
import yaml
import os

def parse_args():
    parser = argparse.ArgumentParser(description="Configure lusid_express settings.")
    parser.add_argument('-e','--enable', nargs='+', type=str, choices=['vars', 'magic'], help='Enable feature(s).')
    parser.add_argument('-d','--disable', nargs='+', type=str, choices=['vars', 'magic'], help='Disable feature(s).')
    return parser.parse_args()

def update_config(args):
    config_path = os.path.join(os.path.dirname(__file__), 'config.yaml')
    if os.path.exists(config_path):
        with open(config_path, 'r') as f:
            config = yaml.safe_load(f) or {}
    else:
        config = {'features': []}

    enabled_features = set(config.get('features', []))

    if args.enable:
        enabled_features.update(args.enable)

    if args.disable:
        enabled_features.difference_update(args.disable)

    config['features'] = list(enabled_features)

    with open(config_path, 'w') as f:
        yaml.safe_dump(config, f)

def main():
    args = parse_args()
    update_config(args)
    print("Configuration updated successfully! Changes will be applied after kernel restart.")

if __name__ == "__main__":
    main()
